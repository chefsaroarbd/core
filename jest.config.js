module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    collectCoverage: true,
    reporters: [
        'default',
        ['jest-junit', {
            'outputDirectory': 'coverage'
        }]
    ],
    coverageReporters: ['text', 'cobertura'],
    testResultsProcessor: 'jest-junit',
    coverageDirectory: 'coverage'
};
